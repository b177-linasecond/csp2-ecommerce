const express = require("express")
	  router = express.Router()
	  prodController = require("../controllers/product")
	  auth = require("../auth")

router.post("/products", auth.verify, (req, res) => {
	let data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	prodController.createProduct(data).then(result => res.send(result))
})

router.get("/products", (req, res) => {
	prodController.retrieveActiveProducts().then(result => res.send(result))
})

router.get("/products/:productId", (req, res) => {
	prodController.specificProduct(req.params).then(result => res.send(result))
})

router.put("/products/:productId", auth.verify, (req, res) => {
	let data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	prodController.updateProduct(req.params, data).then(result => res.send(result))
})

router.put("/products/:productId/archive", auth.verify, (req, res) => {
	let data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	prodController.archiveProduct(req.params, data).then(result => res.send(result))
})

router.get("/allProducts", (req, res) => {
	prodController.getAllProducts().then(result => res.send(result))
})


module.exports = router