const express = require("express")
	  router = express.Router()
	  orderController = require("../controllers/order")
	  auth = require("../auth")

router.post("/checkout", auth.verify, (req, res) => {
	const reqBody = {
		order : req.body,
		id : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	orderController.checkout(reqBody).then(result => res.send(result))
})

router.get("/orders", auth.verify, (req, res) => {
	const reqBody = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	orderController.allOrders(reqBody).then(result => res.send(result))
})

router.get("/myOrders", auth.verify, (req, res) => {
	const reqBody = {
		id : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	orderController.myOrder(reqBody).then(result => res.send(result))
})
module.exports = router