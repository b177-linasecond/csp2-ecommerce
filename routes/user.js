const express = require("express")
	  router = express.Router()
	  userController = require("../controllers/user")
	  auth = require("../auth")

// User registration
router.post("/register", (req, res) => {
	userController.userRegister(req.body).then(result => res.send(result))
})

router.post("/login", (req, res) => {
	userController.userLogin(req.body).then(result => res.send(result))
})

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	let reqBody = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.setUserAsAdmin(req.params, reqBody).then(result => res.send(result))
})

router.get("/users", (req, res) => {
	userController.getAllUsers().then(result => res.send(result))
})

module.exports = router