const Order = require("../models/Order")
	  Product = require("../models/Product")
	  bcrypt = require("bcrypt")
	  auth = require("../auth")

module.exports.checkout = (reqBody) => {
	if(!reqBody.isAdmin) {
		let sum = 0
			array = []
		for(let i = 0; i < reqBody.order.orders.length; i++) {
			let sub = reqBody.order.orders[i].quantity * reqBody.order.orders[i].price
			array.push({productId : reqBody.order.orders[i].productId, price : reqBody.order.orders[i].price, quantity : reqBody.order.orders[i].quantity, subTotal : sub})
			sum = sum + (reqBody.order.orders[i].price * reqBody.order.orders[i].quantity)
		}
		
		let newOrder = new Order({
			totalAmount : sum,
			userId : reqBody.id,
			orders : array
		})

		return newOrder.save().then((ord, error) => {
			if(ord) {
				return (`Order successful.`)
			}
			else {
				return (`Couldn't complete order.`)
			}
		})
	}
	else {
		return Promise.resolve(`Couldn't make an order. User is an administrator.`)
	}
}

module.exports.allOrders = (reqBody) => {
	if(reqBody.isAdmin) {
		return Order.find({}).then(result => result)
	}
	else {
		return Promise.resolve(`Cannot access.`)
	}
}

module.exports.myOrder = (reqBody) => {
	if(!reqBody.isAdmin) {
		return Order.find({userId : reqBody.id}).then(result => {
			if(result.length > 0) {
				return Order.find({userId : reqBody.id}).then(result => result)
			}
			else {
				return Promise.resolve(`No orders found.`)
			}
		})
	}
	else {
		return Promise.resolve(`No orders found. User is an administrator.`)
	}
}