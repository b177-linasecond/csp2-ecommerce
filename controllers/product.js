const Product = require("../models/Product")
	  bcrypt = require("bcrypt")
	  auth = require("../auth")

module.exports.createProduct = reqBody => {
	if(reqBody.isAdmin) {
		let newProduct = new Product({
			name : reqBody.product.name,
			description : reqBody.product.description,
			price : reqBody.product.price
		})

		return newProduct.save().then((product, error) => {
			if(product) {
				return (`Product is listed.`)
			}
			else {
				return (`Product not listed.`)
			}
		})	
	}
	else {
		return Promise.resolve(`User is not authorized to list a product.`)
	}
}

module.exports.retrieveActiveProducts = () => {
	return Product.find({isActive : true}).then(result => result)
}

module.exports.specificProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		if(result) {
			return result
		}
		else {
			return Promise.resolve(`Product not found.`)
		}
	})
}

module.exports.updateProduct = (reqParams, reqBody) => {
	if(reqBody.isAdmin) {
		let updatedProd = {
			name : reqBody.product.name,
			description : reqBody.product.description,
			price : reqBody.product.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProd).then((product, error) => {
			if(product) {
				return (`Product is updated.`)
			}
			else {
				return (`Cannot update product.`)
			}
		})
	}
	else {
		return Promise.resolve(`User is not authorized to update product.`)
	}
}

module.exports.archiveProduct = (reqParams, reqBody) => {
	if(reqBody.isAdmin) {
		let updatedProd = {
			isActive : false
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProd).then((product, error) => {
			if(product) {
				return (`Product is archived.`)
			}
			else {
				return (`Cannot archive product.`)
			}
		})
	}
	else {
		return Promise.resolve(`User is not authorized to delete product.`)
	}
}

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => result)
}