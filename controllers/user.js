const User = require("../models/User")
	  bcrypt = require("bcrypt")
	  auth = require("../auth")

module.exports.userRegister = data => {
	let reqBody = {
		email : data.email.toLowerCase(),
		password : data.password
	}
	if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(reqBody.email)) {
		return User.find({}).then(result => {
			if(result.length > 0) {
				return User.find({email : reqBody.email}).then(result => {
					if(result.length > 0) {
						return (`Email is already registered.`)
					}
					else {
						let newUser = new User({
							email : reqBody.email,
							password : bcrypt.hashSync(reqBody.password, 10)
						})

						return newUser.save().then((user, error) => {
							if(error) {
								return false
							}
							else {
								return (`Email successfully registered.`)
							}
						})
					}
				})
			}
			else {
				let newUser = new User({
					email : reqBody.email,
					password : bcrypt.hashSync(reqBody.password, 10),
					isAdmin : true
				})

				return newUser.save().then((user, error) => {
					if(error) {
						return false
					}

					else {
						return (`Admin user created.`)
					}
				})
			}
		})
	}
	else {
		return Promise.resolve(`Please enter a valid email.`)
	}
}

module.exports.userLogin = data => {
	let reqBody = {
		email : data.email.toLowerCase(),
		password : data.password
	}
	if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(reqBody.email)) {
		return User.findOne({email : reqBody.email}).then(result => {
			if(result) {
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

				if(isPasswordCorrect) {
					return {access : auth.createAccessToken(result)}
				}
				else {
					return (`Password is incorrect.`)
				}
			}
			else {
				return (`Email is not registered.`)
			}

		})
	}
	else {
		return Promise.resolve(`Please enter a valid email.`)
	}
}

module.exports.setUserAsAdmin = (reqParams, reqBody) => {
	if(reqBody.isAdmin) {

		return User.findById(reqParams.userId).then(result => {
			if(result.isAdmin) {
				return (`User is already an admin.`)
			}
			else {
				let userToAdmin = {
					isAdmin : true
				}

				return User.findByIdAndUpdate(reqParams.userId, userToAdmin).then((admin, error) => {
					if(error) {
						return false
					}
					else {
						return (`User is now an admin.`)
					}
				})
			}
		})
		
	}
	else {
		return Promise.resolve(`User is not authorized to assign an administrator.`)
	}
}

module.exports.getAllUsers = () => {
	return User.find({}).then(result => result)
}