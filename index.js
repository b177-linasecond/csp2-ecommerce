const express = require("express")
	  mongoose = require("mongoose")
	  cors = require("cors")
	  app = express()
	  userRoute = require("./routes/user")
	  prodRoute =require("./routes/product")
	  orderRoute = require("./routes/order")

mongoose.connect("mongodb+srv://admin:admin@csp2-ecommerce.haieo.mongodb.net/eCommerceAPI?retryWrites=true&w=majority",{
		useNewUrlParser : true,
		useUnifiedTopology : true
})

mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"))

app.use(express.json())
app.use(express.urlencoded({extended : true}))
app.use(cors())
app.use("/", userRoute)
app.use("/", prodRoute)
app.use("/users", orderRoute)

app.listen(process.env.PORT || 4000, () => console.log(`API is now online on port ${process.env.PORT || 4000}`))