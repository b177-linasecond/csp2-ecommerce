const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({
	totalAmount : {
		type : Number,
		required : [true, "Total amount is required"]
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	},
	userId : {
		type : String,
		required : [true, "User ID is required"]
	},
	orders : [{
		productId : {
			type : String,
			required : [true, "Product ID is required"]
		},
		price : {
			type : Number,
			required : [true, "Quantity is required"]
		},
		quantity : {
			type : Number,
			required : [true, "Quantity is required"]
		},
		subTotal : {
			type : Number,
			required : [true, "Subtotal is required"]
		}
	}]	
})

module.exports = mongoose.model("Order", orderSchema)